import { Component, OnInit } from '@angular/core';
import { RemoteService, User } from 'src/app/services/remote.service';
import { AlertController } from '@ionic/angular';

@Component({
  selector: 'app-registro',
  templateUrl: './registro.page.html',
  styleUrls: ['./registro.page.scss'],
})
export class RegistroPage implements OnInit {
  nombre: string;
  user_name: string;
  user_password: string;
  user: User;
  users: any;

  buttonColor = "#";

  constructor(public remoteService: RemoteService , private alertCtrl: AlertController) { }

  ngOnInit() {
  }

  getUser(id){
    this.remoteService.getUser(id)
    .subscribe( response => {
      this.user =  response[0];
      if( response[0] != undefined){
        this.realVal(false);
      }
      else{
        this.realVal(true);
      }     
    });
  }

  postUser(){
    this.remoteService.postUser(this.nombre , this.user_name , this.user_password)
    .subscribe(res => console.log(res), err => console.log(err));
  }

  async validate(){

    if(this.nombre != undefined && this.user_name != undefined && this.user_password != undefined && this.nombre != "" && this.user_name != "" && this.user_password != "")
      await this.getUser(this.user_name);
    else{
      this.alertController("Campos vacios");
      console.log("Campos vacios");
    }
     
  }

  realVal(bol){
    if(bol){
      
      this.postUser();
      this.inicioSesion();
      
    }else{
      this.alertController("Usuario ya existe");
      console.log("usuario ya existe")
    }
  }

  inicioSesion(){
    
    console.log("redirigiendo a home")
  }

  async alertController(texto) {
    var alert = await this.alertCtrl.create({
      header: 'Error',
      message: texto,
      buttons: ['OK']
    });

    await alert.present();
  }

}
