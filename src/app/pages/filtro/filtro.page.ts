import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { Receta, User, RemoteService } from '../../services/remote.service';
import { DataLocalService } from '../../services/data-local.service';

@Component({
  selector: 'app-filtro',
  templateUrl: './filtro.page.html',
  styleUrls: ['./filtro.page.scss'],
})
export class FiltroPage implements OnInit {

  recetas: Receta[] = [];
  likes: any[] = [];
  user: User = new User;

  constructor(private route: ActivatedRoute,
              private router: Router,
              private dataLocalService: DataLocalService,
              private remoteService: RemoteService,
  ) {
    this.route.queryParams.subscribe(params => {
      if (this.router.getCurrentNavigation().extras.state) {
        this.recetas = this.router.getCurrentNavigation().extras.state.parametros;
      }
    });
    this.getLikes();
  }

  ngOnInit() {

  }

  unlike(receta) {
    this.dataLocalService.unlikeReceta(receta);
    this.getLikes();
  }
  like(receta) {
    this.dataLocalService.likeReceta(receta);
    this.getLikes();
  }
  async getLikes() {
    const user = await this.dataLocalService.storage.get('usuario');
    if (user.user_name != undefined && this.user.user_name != "") {
       this.remoteService.getLikes(user.user_name)
      .subscribe(results => {
        for(let i=0; i<results.length; i++) {
          this.likes.push(results[i].id_receta);
        }
      });
      
    }
  }
  isLiked(id_receta: string) {
    if (this.likes.includes(id_receta)) {
      return true;
    } else {
      return false;
    }
  }

}
