import { Component } from '@angular/core';
import { DataLocalService } from 'src/app/services/data-local.service';
import { AlertController, ToastController } from '@ionic/angular';


type RecetaPlan = {
  id_receta: string;
  titulo: string;
  url_foto: string;
  dificultat: string;
  tiempo: string;
  kcal: string;
  diaSemana: string;
}

@Component({
  selector: 'app-tab3',
  templateUrl: 'tab3.page.html',
  styleUrls: ['tab3.page.scss']
})



export class Tab3Page {

  myDate: String = new Date().toISOString();
  slideOptions = {
    initialSlide: 0,
    speed: 300
  };

  dias: string[] = ["Lunes", "Martes", "Miércoles", "Jueves", "Viernes", "Sábado", "Domingo"];  
  
  constructor( private dataLocalService: DataLocalService,
               private toastController: ToastController,
               private alertController: AlertController,
  ) {
    var fecha = new Date();
    
    this.slideOptions.initialSlide = fecha.getDay()-1;
  }


  //Pasamos los id de la receta para la página de detalle
  detalle(receta_id , id) {
    this.dataLocalService.detalle(receta_id, id);
  }

  async borrarTodo() {
    this.dataLocalService.borrarTodo("planSemanal");
  }

  async confirmDelete(receta: RecetaPlan) {
    const alert = await this.alertController.create({
      cssClass: 'my-custom-class',
      header: 'Eliminar receta',
      message: '¿Quieres <strong>eliminar</strong> la receta del plan semanal?',
      buttons: [
        {
          text: 'Cancelar',
          role: 'cancel',
          cssClass: 'secondary',
          handler: () => {
          }
        }, {
          text: 'Eliminar',
          handler: () => {
            this.dataLocalService.unplanReceta(receta);
          }
        }
      ]
    });

    await alert.present();
  }

  izq(receta: RecetaPlan, diaActual: string) {
    if (diaActual != "Lunes") { //Si la receta se puede subir
      //Buscamos qué receta es
      let found = false;
      let i = 0;
      while (!found) {
        if(this.dataLocalService.planSemanal[i] == receta) {
          found = true;
        } else {
          i++;
        }
      }
      
      var dia = 0;
      let diaFound = false;
      while (!diaFound) {
        if (this.dataLocalService.planSemanal[i].diaSemana === this.dias[dia]) {
          this.dataLocalService.planSemanal[i].diaSemana = this.dias[dia-1]; //Cambiamos el dia de la receta
          diaFound = true;
          this.dataLocalService.recetasPorDias[dia]--;
          this.dataLocalService.recetasPorDias[dia-1]++;
        } else {
           dia++;
         }
      }
      this.toast(receta.titulo, dia-1);
      this.dataLocalService.syncPlan();
    }
  } 

  derecha(receta: RecetaPlan, diaActual: string) {
    if (diaActual != "Domingo") {  //Si la receta se puede bajar
      let found = false;
      let i = 0;
      while (!found) {
        if(this.dataLocalService.planSemanal[i] == receta) {
          found = true;
        } else {
          i++;
        }
      }
      var dia = 0;
      let diaFound = false;
      while (!diaFound) {
        if (this.dataLocalService.planSemanal[i].diaSemana == this.dias[dia]) {
          this.dataLocalService.planSemanal[i].diaSemana = this.dias[dia+1]; //Cambiamos el dia de la receta
          diaFound = true;
          this.dataLocalService.recetasPorDias[dia]--;
          this.dataLocalService.recetasPorDias[dia+1]++;
        } else {
           dia++;
         }
      }
      this.toast(receta.titulo, dia+1);
      this.dataLocalService.syncPlan();
    }
  }

  async toast(titulo: string, dia: number) {
    console.log(this.dias[dia]);
    const toast = await this.toastController.create({
      message: "<strong>" + titulo + "</strong> se ha movido al <strong>" + this.dias[dia] + "</strong>",
      duration: 1200,
      color: "primary"
    });
    toast.present();
  }
}
