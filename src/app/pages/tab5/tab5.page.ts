import { ViewChild } from '@angular/core';
import { Component, OnInit } from '@angular/core';
import { AlertController, NavController, ToastController } from '@ionic/angular';
import { Router } from '@angular/router';
import { RemoteService, User } from 'src/app/services/remote.service';
import { DataLocalService } from 'src/app/services/data-local.service';

@Component({
  selector: 'app-tab5',
  templateUrl: './tab5.page.html',
  styleUrls: ['./tab5.page.scss'],
})
export class Tab5Page implements OnInit {

  @ViewChild('myNav') nav: NavController;

  buttonColor = "#";

  user: User;
  //
  user_name: string;
  user_password: string;

  constructor(public route: Router,
              public navController: NavController,
              public remoteService: RemoteService,
              private alertCtrl: AlertController,
              private dataLocalService: DataLocalService,
              private toastController: ToastController,
  ) {
    //this.loggedIn = this.dataLocalService.loggedIn;
  }

  async inicioSesion() {    
    this.dataLocalService.logIn(this.user);
    const toast = await this.toastController.create({
      message: "Sesión iniciada con éxito",
      duration: 1100,
      color: "success",
    });
    toast.present();
  }
  async cerrarSesion() {
    this.dataLocalService.cerrarSesion();
    const toast = await this.toastController.create({
      message: "Sesión cerrada con éxito",
      duration: 1100,
      color: "primary",
    });
    toast.present();
  }

  ngOnInit() {
  }

  navigate() {
    this.route.navigate(['/registro']);
  }

  getUser(id){
    this.remoteService.getUser(id)
    .subscribe( response => {
      this.user =  response[0];
      if( response[0] != undefined){
        this.realVal(true);
      }
      else{
        this.realVal(false);
      }     
    });
  }

  comprobarContra(){
    var pass = false;
    if(this.user_password == this.user.user_password){
      pass = true
    }
    return pass;
  }

  async validate(){
    
    await this.getUser(this.user_name);
  }

  realVal(bol){
    if(bol){
      if( this.comprobarContra()){

        this.inicioSesion();
      }else{
        this.alertController("Contraseña Erronea");
        console.log("Contraseña Erronea")
      }
    }else{
      this.alertController("Usuario no existe");
      console.log("usuario no existente")
    }
  }

  async alertController(texto) {
    var alert = await this.alertCtrl.create({
      header: 'Error',
      message: texto,
      buttons: ['OK']
    });

    await alert.present();
  }

}
