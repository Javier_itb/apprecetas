import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { Pasos } from 'src/app/services/remote.service';

@Component({
  selector: 'app-pasos',
  templateUrl: './pasos.page.html',
  styleUrls: ['./pasos.page.scss'],
})
export class PasosPage implements OnInit {

  indice: number = 0;
  data: Pasos[];
  paso;
  numPasos: number;

  constructor(private route: ActivatedRoute, private router: Router) {
    this.route.queryParams.subscribe(params => {
      if (this.router.getCurrentNavigation().extras.state) {

        this.data = this.router.getCurrentNavigation().extras.state.parametros[0];
        this.paso = this.data[this.indice];
        
        this.numPasos = this.data.length;                
      }
    })
  }

  ngOnInit() {
  }
  
  nextPaso() {
    if (this.paso.numPaso < this.numPasos) {
      this.indice++;
      this.paso = this.data[this.indice];
    }
  }

  prevPaso() {
    if (this.paso.numPaso > 1) {
      this.indice--;
      this.paso = this.data[this.indice];
    }
  }
}
