import { Component, Input } from '@angular/core';
import { Receta, RemoteService, User } from '../../services/remote.service';
import { DataLocalService } from '../../services/data-local.service';
import { ActionSheetController, ToastController, AlertController } from '@ionic/angular';
import { Router, NavigationExtras } from '@angular/router';

@Component({
  selector: 'app-tab4',
  templateUrl: 'tab4.page.html',
  styleUrls: ['tab4.page.scss'],
})
export class Tab4Page {

  likes: any[] = [];
  user: User = new User;

  constructor(public dataLocalService: DataLocalService,
              public remoteService: RemoteService,
              public actionSheetController: ActionSheetController,
              public route: Router,
              public toastController: ToastController,
              public alertController: AlertController,
  ) {
    this.getLikes();
  }

  ngOnInit() {
  }

  async borrarTodo() {
    this.dataLocalService.borrarTodo("favoritos");
  }

  //Pasamos los id de la receta para la página de detalle
  detalle(receta_id , id) {
    this.dataLocalService.detalle(receta_id, id);
  }

  unlike(receta) {
    this.dataLocalService.unlikeReceta(receta);
    this.getLikes();
  }
  like(receta) {
    this.dataLocalService.likeReceta(receta);
    this.getLikes();
  }
  async getLikes() {
    const user = await this.dataLocalService.storage.get('usuario');
    if (user.user_name != undefined && this.user.user_name != "") {
       this.remoteService.getLikes(user.user_name)
      .subscribe(results => {
        for(let i=0; i<results.length; i++) {
          this.likes.push(results[i].id_receta);
        }
      });
      
    }
  }
  isLiked(id_receta: string) {
    if (this.likes.includes(id_receta)) {
      return true;
    } else {
      return false;
    }
  }

}
