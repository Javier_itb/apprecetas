import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, NavigationExtras, Router } from '@angular/router';
import { Ingredientes, Pasos, Receta, RemoteService } from 'src/app/services/remote.service';
import { DataLocalService } from '../../services/data-local.service';
import { ActionSheetController, ToastController } from '@ionic/angular';
import { User } from '../../services/remote.service';

@Component({
  selector: 'app-detalle',
  templateUrl: './detalle.page.html',
  styleUrls: ['./detalle.page.scss'],
})
export class DetallePage implements OnInit {

  buttonColor = "#7fffd4";

  id: any;        //id de la receta (generada automáticamente por mongodb)
  receta_id: any; //id de la receta (generada manualmente)
  receta: Receta = new Receta();
  pasosReceta: Pasos[];
  ingredientes: Ingredientes[];

  likes: any[] = [];
  user: User = new User;

  constructor(private route: ActivatedRoute,
              private router: Router,
              private remoteService: RemoteService,
              private dataLocalService: DataLocalService,
              private actionSheetController: ActionSheetController,
              private toastController: ToastController,
  ) {
    this.route.queryParams.subscribe(params => {
      if (this.router.getCurrentNavigation().extras.state) {

        this.receta_id = this.router.getCurrentNavigation().extras.state.parametros[0];
        this.id = this.router.getCurrentNavigation().extras.state.parametros[1];
      }
      this.getLikes();
    });
  }

  ngOnInit() {
    this.getReceta(this.id);
    this.getPasos(this.receta_id);
    this.getIngredientes(this.receta_id);
  }

  ionViewWillEnter() {
    this.getLikes();
  }

  
  //Llamadas a la API
  //para recuperar la receta
  getReceta(id){
    this.remoteService.getRecetaById(id)
    .subscribe(results => {
      this.receta = results;
    })
  }
  //para recuperar sus pasos
  getPasos(id){
    this.remoteService.getPasosReceta(id)
    .subscribe(results => {
      this.pasosReceta = results;
    })
  }
  //para recuperar sus ingredientes
  getIngredientes(id){
    this.remoteService.getIngredientesReceta(id)
    .subscribe(results => {
      this.ingredientes = results;
    })
  }

  //pasamos los datos de los pasos de la receta a la página 'pasos'
  pasos() {  
    var navigationExtras: NavigationExtras = {
      state: {
        parametros: [this.pasosReceta]
      }
    }
    this.router.navigate(['/pasos'], navigationExtras);
  }


  unlike(receta) {
    this.dataLocalService.unlikeReceta(receta);
    this.getLikes();
  }
  like(receta) {
    this.dataLocalService.likeReceta(receta);
    this.getLikes();
  }
  async getLikes() {
    const user = await this.dataLocalService.storage.get('usuario');
    if (user.user_name != undefined && this.user.user_name != "") {
       this.remoteService.getLikes(user.user_name)
      .subscribe(results => {
        for(let i=0; i<results.length; i++) {
          this.likes.push(results[i].id_receta);
        }
      });
      
    }
  }
  isLiked(id_receta: string) {
    if (this.likes.includes(id_receta)) {
      return true;
    } else {
      return false;
    }
  }
}
