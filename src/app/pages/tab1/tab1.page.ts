import { Component , Input, OnInit } from '@angular/core';
import { NavigationExtras, Router } from '@angular/router';
import { ActionSheetController, AlertController, ToastController } from '@ionic/angular';
import { RemoteService, User } from 'src/app/services/remote.service';
import { DataLocalService } from '../../services/data-local.service';
import { Receta } from '../../services/remote.service';


@Component({
  selector: 'app-tab1',
  templateUrl: 'tab1.page.html',
  styleUrls: ['tab1.page.scss']
})

export class Tab1Page implements OnInit {

  recetas: Receta[] = [];
  page: number = 0;
  moreData: boolean = true;
  likes: any[] = [];
  user: User = new User;

  //https://www.djamware.com/post/59924f9080aca768e4d2b12e/ionic-3-consuming-rest-api-using-new-angular-43-httpclient
  //https://github.com/vasconce7os/ionic3_angular_httpclient
  //https://www.youtube.com/watch?v=3NmzfhvBo24&ab_channel=nicobytes

  constructor(private route: Router,
              public actionSheetController: ActionSheetController,
              public remoteService: RemoteService,
              public dataLocalService: DataLocalService,
              public toastController: ToastController,
              public alertController: AlertController,
              ) {
    this.getRecetas();
    this.getLikes();
  }
  ngOnInit() {
  }

  ionViewWillEnter() {
    this.getLikes();
  }

  //Recuperamos las recetas de la API
  getRecetas(){
    this.remoteService.getRecetas(this.page)
    .subscribe(results => {
      this.recetas = this.recetas.concat(results);
      if (results.length < 5) {
        this.moreData = false;
      }
    })
  }

  //Pasamos los id de la receta para la página de detalle
  detalle(receta_id , id) {
    this.dataLocalService.detalle(receta_id, id);
  }


  unlike(receta) {
    this.dataLocalService.unlikeReceta(receta);
    this.getLikes();
  }
  like(receta) {
    this.dataLocalService.likeReceta(receta);
    this.getLikes();
  }
  async getLikes() {
    const user = await this.dataLocalService.storage.get('usuario');
    if (user.user_name != undefined && this.user.user_name != "") {
       this.remoteService.getLikes(user.user_name)
      .subscribe(results => {
        for(let i=0; i<results.length; i++) {
          this.likes.push(results[i].id_receta);
        }
      });
      
    }
  }
  isLiked(id_receta: string) {
    if (this.likes.includes(id_receta)) {
      return true;
    } else {
      return false;
    }
  }
  
  
  loadData(event) {
    this.page+=5;
    this.getRecetas();
  }

}
