import { Component } from '@angular/core';
import { NavigationExtras, Router } from '@angular/router';
import { RemoteService, Receta } from '../../services/remote.service';

@Component({
  selector: 'app-tab2',
  templateUrl: 'tab2.page.html',
  styleUrls: ['tab2.page.scss']
})
export class Tab2Page {

  buttonColor = "#"

  constructor(private route: Router,
              private remoteService: RemoteService,
  ) {}

  recetas: Receta[];
  titulo: string;

  async filtrar(alimento: string) {
    await this.remoteService.getRecetasByAlimento(alimento)
    .subscribe(results => {
       this.recetas = results;

       var navigationExtras: NavigationExtras = {
        state: {
          parametros: results
        }
      }
      this.route.navigate(['filtro'], navigationExtras);
    });
  }

  async filtrarTag(tag: string) {
    await this.remoteService.getRecetasByTag(tag)
    .subscribe(results => {
       this.recetas = results;

       var navigationExtras: NavigationExtras = {
        state: {
          parametros: results
        }
      }
      this.route.navigate(['filtro'], navigationExtras);
    });
  }

  async buscar() {
    if (this.titulo != undefined) {
      await this.remoteService.getRecetasByTitulo(this.titulo)
      .subscribe(results => {
         this.recetas = results;
  
         var navigationExtras: NavigationExtras = {
          state: {
            parametros: results
          }
        }
        this.route.navigate(['filtro'], navigationExtras);
      });
    } else {
      console.log("No se ha introducido ningun nombre");
    }
  }
  
}
var posibles_tags: string[] = [
    "Vegetariano","Vegano","Celíaco","Sin lactosa"
]