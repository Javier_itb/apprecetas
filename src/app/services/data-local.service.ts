import { Injectable } from '@angular/core';
import { NavigationExtras, Router } from '@angular/router';
import { ActionSheetController, AlertController, ToastController } from '@ionic/angular';
import { Storage } from '@ionic/storage-angular';
import { Likes, Receta, RemoteService, User } from './remote.service';

@Injectable({
  providedIn: 'root'
})
export class DataLocalService {

  //Array con todas las recetas marcadas como favoritas
  favoritos: Receta[] = [];
  //Array con todas las recetas incluidas en el plan semanal
  planSemanal: {
    id_receta: string;
    titulo: string;
    url_foto: string;
    dificultat: string;
    tiempo: string;
    kcal: string;
    diaSemana: string;
  }[] = [];
  //Cantidad de recetas que hay en cada día del plan semanal
  recetasPorDias: number[] = [0,0,0,0,0,0,0];

  loggedIn: boolean = false;
  usuario: User = new User;

  likes: any[] = [];
  

  constructor(public storage: Storage, 
              private alertController: AlertController,
              private route: Router,
              private toastController: ToastController,
              private remoteService: RemoteService,
  ) {
    this.storage.create();
    this.cargarFavoritos();
    this.cargarPlan();
    this.cargarUsuario();

    
  }


  async likeReceta(receta) {
    if (this.loggedIn) {
      this.remoteService.like(receta._id).subscribe();
      this.remoteService.postLike(this.usuario.user_name, receta.id_receta).subscribe();
    } else {
      const toast = await this.toastController.create({
        message: "Debes iniciar sesión para darle a <i>me gusta</i>",
        duration: 1300,
        color: "warning",
      });
      toast.present();
    }
  }
  unlikeReceta(receta) {
    if (this.loggedIn) {
      this.remoteService.unlike(receta._id).subscribe();
      this.remoteService.deleteLike(this.usuario.user_name, receta.id_receta).subscribe();
    }
  }

  async getLikes() {
    const user = await this.storage.get('usuario');
    if (user.user_name != undefined && this.usuario.user_name != "") {
       this.remoteService.getLikes(user.user_name)
      .subscribe(results => {
        for(let i=0; i<results.length; i++) {
          this.likes.push(results[i].id_receta);
        }
      });
      
    }
  }

  async cargarUsuario() {
    const usuario = await this.storage.get('usuario');
    const loggedIn = await this.storage.get('loggedIn');
    if (loggedIn) {
      this.loggedIn = true;
    } else {
      this.loggedIn = false;
    }
    if (usuario) {
      this.usuario = usuario;
    }
    return this.usuario;
  }

  async logIn(user: User) {
    this.usuario.nombre = user.nombre;
    this.usuario.user_name = user.user_name;
    this.usuario.user_password = user.user_password;
    
    this.loggedIn = true;

    await this.storage.set('usuario', this.usuario);
    await this.storage.set('loggedIn', true);

    this.remoteService.getLikes(this.usuario.user_name)
    .subscribe(results => {
      for(let i=0; i<results.length; i++) {
        this.likes.push(results[i].id_receta);
      }
      this.storage.set('likes', this.likes);
    });
    this.cargarUsuario();
  }
/*
  async isLiked(id_receta: string) {
    console.log(id_receta);
    //console.log(this.storage.get('likes'));
    const likes = await this.storage.get('likes');
    if (likes.includes(id_receta)) {
      console.log("está liked");
      return true;
    } else {
      //console.log("no está liked");
      return false;
    }
  }*/

  cerrarSesion() {
    this.usuario = new User;
    this.storage.set('usuario', this.usuario);
    this.storage.set('loggedIn', false);
    this.loggedIn = false;
  }

  //Guardar una sola receta en favoritos
  favReceta(receta: Receta) {
    this.favoritos.unshift(receta);
    this.storage.set('favoritos', this.favoritos);
    this.estado(false, "favoritos");
  }
  //Borrar una sola receta de favoritos
  unfavReceta(receta: Receta) {
    this.favoritos = this.favoritos.filter(r => r.titulo !== receta.titulo);
    this.storage.set('favoritos', this.favoritos);
    this.estado(true, "favoritos");
  }


  //Guardar una sola receta al plan semanal
  planReceta(receta) {
    this.selectDia(receta);
  }
  guardarPlanReceta(receta) {
    this.planSemanal.unshift(receta);
    this.storage.set('planSemanal', this.planSemanal);
    this.modificarRecetasPorDias(receta, "sumar");
  }
  //Borrar una sola receta del plan semanal
  unplanReceta(receta) {
    this.planSemanal = this.planSemanal.filter(r => r.titulo !== receta.titulo);
    this.storage.set('planSemanal', this.planSemanal);
    this.modificarRecetasPorDias(receta, "restar");
    this.estado(true, "planSemanal");
  }
  syncPlan() {
    this.storage.set('planSemanal', this.planSemanal);
  }


  //Borrar todas las recetas de un array
  async borrarTodo(array: string) {
    //modificamos los mensajes de alerta y de éxito según lo que se vaya a eliminar
    var msgAlerta: string;
    var msgSuccess: string;
    if (array == "favoritos") {
      msgAlerta = '<strong>TODAS</strong> tus recetas favoritas?';
      msgSuccess = 'Todas tus recetas favoritas se han';
    } else if (array == "planSemanal") {
      msgAlerta = '<strong>TODO</string> tu plan semanal?';
      msgSuccess = 'Todo tu plan semanal se ha';
    }
    //mostramos una alerta para confirmar el borrado
    const alert = await this.alertController.create({
      cssClass: 'my-custom-class',
      header: 'Alerta',
      message: '¿Quieres borrar ' + msgAlerta,
      buttons: [
        {
          text: 'Cancelar',
          role: 'cancel',
          cssClass: 'secondary',
          handler: () => {
          }
        }, {
          text: 'Borrar todo',
          handler: async () => {
            //Borramos el array
            if (array == "favoritos") {
              this.favoritos = [];
            } else if (array == "planSemanal") {
              this.planSemanal = [];
            }
            this.storage.set(array, []);

            //mostramos un mensaje de éxito
            const toast = await this.toastController.create({
            message: msgSuccess + " eliminado con éxito",
              duration: 1000,
              color: "success"
            });
            toast.present();
          }
        }
      ]
    });

    await alert.present();
  }
  //Cargamos el array de los favoritos
  async cargarFavoritos() {
    const favoritos = await this.storage.get('favoritos');
    if (favoritos) {
      this.favoritos = favoritos;
    }
    return this.favoritos;
  }
  //Cargamos el array del plan semanal
  async cargarPlan() {
    const plan = await this.storage.get('planSemanal');
    if (plan) {
      this.planSemanal = plan;
    }
    for (let i=0; i<this.planSemanal.length; i++) {
      this.modificarRecetasPorDias(this.planSemanal[i], "sumar");
    }
    return this.planSemanal;
  }

  //Comprovamos si una receta ya está en favoritos
  isFavorito(titulo: string) {
    var fav: boolean = false;
    var i: number = 0;

    while (!fav && i<this.favoritos.length) {
      if (titulo == this.favoritos[i].titulo) {
        fav = true;
      } else {
        i++;
      }
    }
    return fav;
  }
  //Comprovamos si una receta ya está en el plan semanal
  isOnPlan(titulo: string) {
    var planned: boolean = false;
    var i: number = 0;

    while (!planned && i<this.planSemanal.length) {
      if (titulo == this.planSemanal[i].titulo) {
        planned = true;
      } else {
        i++;
      }
    }
    return planned;
  }


  //Pasamos los id de una receta a la página de detalle y navegamos a ella
  detalle(receta_id , id) {
    var navigationExtras: NavigationExtras = {
      state: {
        parametros: [receta_id , id ]
      }
    };
    this.route.navigate(['/detalle'], navigationExtras);
  }


  //Generación de un "toast" para informar el cambio al usuario
  async estado(guardada: boolean, array: string) {
    var msg: string;
    var color: string;
    if (guardada) {
      if (array == "favoritos")
        msg = 'Receta eliminada de favoritos';
      else 
        msg = 'Receta eliminada del plan semanal';

      color = 'primary';
    } else  {
      if (array == "favoritos")
        msg = 'Receta guardada en favoritos';
      else 
        msg = 'Receta añadida al plan semanal';
      
      color = 'success';
    }
    
    const toast = await this.toastController.create({
      message: msg,
      duration: 800,
      color: color
    });
    toast.present();
  }

  //Alerta para que el usuario seleccione a qué día del plan semanal quiere añadir una receta
  async selectDia(receta) {
    const alert = await this.alertController.create({
      cssClass: 'my-custom-class',
      header: 'Añadiendo al plan semanal',
      subHeader: 'Selecciona el dia:',
      buttons: [
        {
          text: 'Lunes',
          handler: () => {
            receta["diaSemana"] = "Lunes";
            this.guardarPlanReceta(receta);
            this.estado(false, "planSemanal");
          }
        }, {
          text: 'Martes',
          handler: () => {
            receta["diaSemana"] = "Martes";
            this.guardarPlanReceta(receta);
            this.estado(false, "planSemanal");
          }
        }, {
          text: 'Miercoles',
          handler: () => {
            receta["diaSemana"] = "Miércoles";
            this.guardarPlanReceta(receta);
            this.estado(false, "planSemanal");
          }
        }, {
          text: 'Jueves',
          handler: () => {
            receta["diaSemana"] = "Jueves";
            this.guardarPlanReceta(receta);
            this.estado(false, "planSemanal");
          }
        }, {
          text: 'Viernes',
          handler: () => {
            receta["diaSemana"] = "Viernes";
            this.guardarPlanReceta(receta);
            this.estado(false, "planSemanal");
          }
        }, {
          text: 'Sábado',
          handler: () => {
            receta["diaSemana"] = "Sábado";
            this.guardarPlanReceta(receta);
            this.estado(false, "planSemanal");
          }
        }, {
          text: 'Domingo',
          handler: () => {
            receta["diaSemana"] = "Domingo";
            this.guardarPlanReceta(receta);
            this.estado(false, "planSemanal");
          }
        },{
          text: '',
        }, {
          text: 'Cancel',
          role: 'cancel',
          cssClass: 'secondary',
          handler: () => {
            
          }
        }
      ]
    });

    await alert.present();
  }
  //Modificamos la cantidad de recetas que hay en cada día
  modificarRecetasPorDias(receta, accion) {
    var indice: number;
    switch(receta.diaSemana) {
      case "Lunes":
        indice = 0;
        break;
      case "Martes":
        indice = 1;
        break;
      case "Miércoles":
        indice = 2;
        break;
      case "Jueves":
        indice = 3;
        break;
      case "Viernes":
        indice = 4;
        break;
      case "Sábado":
        indice = 5;
        break;
      case "Domingo":
        indice = 6;
        break;
    }
    if (accion == "sumar") {
      this.recetasPorDias[indice]++;
    } else {
      this.recetasPorDias[indice]--;
    }
  }
}
