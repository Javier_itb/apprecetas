import { Injectable } from '@angular/core';
import { HttpClient} from  '@angular/common/http';
import 'rxjs/add/operator/do';
import 'rxjs/add/operator/map';
import 'rxjs/add/operator/catch';
import { Observable } from 'rxjs';
import { catchError } from 'rxjs/operators';

@Injectable({
  providedIn: 'root'
})
export class RemoteService {
  baseUrl: string = "http://enrique-moran-7e3.alwaysdata.net/api";

  constructor(private http : HttpClient) { }

  getRecetas(page: number){
    return this.http.get<Receta[]>(this.baseUrl+'/recetas/'+page);
  }
  public getRecetaById(recetaId: string) {
    return this.http.get<Receta>(this.baseUrl+'/receta/'+recetaId);
  }
  public getPasosReceta(recetaId: string) {
    return this.http.get<Pasos[]>(this.baseUrl+'/pasos/'+recetaId);
  }
  public getIngredientesReceta(recetaId: string) {
    return this.http.get<Ingredientes[]>(this.baseUrl+'/ingredientes/'+recetaId);
  }
  public getRecetasByTitulo(nombre: string) {
    return this.http.get<Receta[]>(this.baseUrl+'/receta/titulo/'+nombre);
  }
  public getRecetasByAlimento(alimento: string) {
    return this.http.get<Receta[]>(this.baseUrl+'/receta/alimento/'+alimento);
  }
  public getRecetasByTag(tag: string) {
    return this.http.get<Receta[]>(this.baseUrl+'/receta/tag/'+tag);
  }
  public getLikes(user: string) {
    return this.http.get<Likes[]>(this.baseUrl+'/likes/'+user);
  }
  public getUser(id: string) {
    return this.http.get<User>(this.baseUrl+'/users/'+id);
  }
  public postUser(nombre: string , user_name: string , user_password: string){
    const fd = new FormData();
    fd.append('nombre', nombre);
    fd.append('user_name', user_name);
    fd.append('user_password', user_password);
    return this.http.post(this.baseUrl+'/users/',fd);
  }

  public postLike(user_name: string , id_receta: string){
    const fd = new FormData();
    fd.append('user_name', user_name);
    fd.append('id_receta', id_receta);
    return this.http.post(this.baseUrl+'/likes/',fd);
  }
  public deleteLike(user_name: string , id_receta: string) {
    return this.http.delete(this.baseUrl+'/like/'+user_name+'/'+id_receta);
  }
  public like(receta_id: string) {
    return this.http.get<Receta>(this.baseUrl+'/receta/like/'+receta_id+'/1');
  }

  public unlike(receta_id: string) {
    return this.http.get<Receta>(this.baseUrl+'/receta/like/'+receta_id+'/-1');
  }

  
}

export class Receta {
  id_receta: string;
  titulo: string
  url_foto: string;
  descripcion: string;
  dificultat: string;
  tiempo: string;
  kcal: string;
  tipo_plato: string;
  tipo_alimento: any;

  constructor(values: Object = {}) {
    Object.assign(this, values);
  }
}

export class Pasos {
  descripcion: any;
  id_receta: string;
  numPaso: string;
  foto: string;

  constructor(values: Object = {}) {
    Object.assign(this, values);
  }
}

export class Ingredientes {
  id_receta: string;
  nombre: string;

  constructor(values: Object = {}) {
    Object.assign(this, values);
  }
}

export class User {
  
  nombre: string;
  user_name: string;
  user_password: string;
  
  constructor(values: Object = {}) {
    Object.assign(this, values);
  }
}

export class Likes {
  
  id_receta: string;
  user_name: string;
  
  constructor(values: Object = {}) {
    Object.assign(this, values);
  }
}

