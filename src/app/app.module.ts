import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { RouteReuseStrategy } from '@angular/router';
import { RemoteService } from './services/remote.service';
import { IonicModule, IonicRouteStrategy } from '@ionic/angular';
import { HttpClientModule } from '@angular/common/http';
import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { IonicStorageModule } from '@ionic/storage-angular';

@NgModule({
  declarations: [AppComponent],
  entryComponents: [],
  imports: [
    BrowserModule,
    IonicModule.forRoot(),
    HttpClientModule , 
    AppRoutingModule,
    IonicStorageModule.forRoot(),
  ],
  providers: [{
    provide: RouteReuseStrategy,
    useClass: IonicRouteStrategy,    
    },
    RemoteService,
  ],
  bootstrap: [AppComponent],
})
export class AppModule {}
